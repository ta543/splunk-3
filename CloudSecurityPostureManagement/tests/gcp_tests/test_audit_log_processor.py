import unittest
from unittest.mock import patch
from gcp.audit_log_processor import process_audit_logs

class TestAuditLogProcessor(unittest.TestCase):

    @patch('gcp.audit_log_processor.logging.Client')
    def test_process_audit_logs(self, mock_logging_client):
        mock_client = mock_logging_client.return_value
        mock_client.list_entries.return_value = [
            {'payload': 'entry1'},
            {'payload': 'entry2'}
        ]
        entries = process_audit_logs()
        self.assertIn('entry1', entries)

if __name__ == '__main__':
    unittest.main()
