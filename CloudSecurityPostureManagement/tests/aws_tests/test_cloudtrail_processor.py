import unittest
from unittest.mock import patch
from aws.cloudtrail_processor import process_cloudtrail_logs

class TestCloudTrailProcessor(unittest.TestCase):

    @patch('aws.cloudtrail_processor.boto3.client')
    def test_process_cloudtrail_logs(self, mock_boto_client):
        mock_client = mock_boto_client.return_value
        mock_client.lookup_events.return_value = {
            'Events': [
                {'CloudTrailEvent': 'Event1'},
                {'CloudTrailEvent': 'Event2'}
            ]
        }
        events = process_cloudtrail_logs()
        self.assertEqual(len(events), 2)
        self.assertIn('Event1', events)

if __name__ == '__main__':
    unittest.main()
