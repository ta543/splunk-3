import unittest
from unittest.mock import patch
from azure.activity_log_processor import process_activity_logs

class TestActivityLogProcessor(unittest.TestCase):

    @patch('azure.activity_log_processor.MonitorManagementClient')
    def test_process_activity_logs(self, mock_monitor_client):
        mock_client = mock_monitor_client.return_value
        mock_client.activity_logs.list.return_value = [
            {'as_dict': lambda: {'event': 'login', 'status': 'success'}},
            {'as_dict': lambda: {'event': 'update', 'status': 'failed'}}
        ]
        logs = process_activity_logs()
        self.assertEqual(len(logs), 2)
        self.assertEqual(logs[0]['event'], 'login')

if __name__ == '__main__':
    unittest.main()
