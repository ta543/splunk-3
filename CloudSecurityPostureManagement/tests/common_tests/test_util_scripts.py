import unittest
from common.util_scripts import pretty_print_json, extract_value

class TestUtilScripts(unittest.TestCase):

    def test_pretty_print_json(self):
        data = {"key1": "value1", "key2": "value2"}
        expected_output = '{\n    "key1": "value1",\n    "key2": "value2"\n}'
        result = pretty_print_json(data)
        self.assertEqual(result, expected_output)

    def test_extract_value(self):
        dict_obj = {"key1": "value1", "key2": "value2"}
        self.assertEqual(extract_value(dict_obj, "key1"), "value1")
        self.assertIsNone(extract_value(dict_obj, "nonexistent_key"))

if __name__ == '__main__':
    unittest.main()
