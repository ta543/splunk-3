def check_system_health():
    return "System is healthy"

def check_network_latency():
    return "Network latency within acceptable range"

if __name__ == "__main__":
    health_status = check_system_health()
    print(health_status)
    latency_status = check_network_latency()
    print(latency_status)
