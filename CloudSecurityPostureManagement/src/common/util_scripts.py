import json

def pretty_print_json(data):
    print(json.dumps(data, indent=4, sort_keys=True))

def extract_value(dict_obj, key):
    return dict_obj.get(key, None)

if __name__ == "__main__":
    test_data = {"key1": "value1", "key2": "value2"}
    pretty_print_json(test_data)
