from azure.identity import DefaultAzureCredential
from azure.mgmt.monitor import MonitorManagementClient

def process_activity_logs():
    credential = DefaultAzureCredential()
    client = MonitorManagementClient(credential, "<your-subscription-id>")
    activity_logs = client.activity_logs.list(
        filter="eventTimestamp ge '2021-01-01T00:00:00Z' and eventTimestamp le '2021-01-02T00:00:00Z'"
    )
    for log in activity_logs:
        print(log.as_dict())

if __name__ == "__main__":
    process_activity_logs()
