import boto3

def process_cloudtrail_logs():
    client = boto3.client('cloudtrail')
    response = client.lookup_events(
        LookupAttributes=[
            {
                'AttributeKey': 'EventName',
                'AttributeValue': 'ConsoleLogin'
            },
        ],
        MaxResults=50
    )
    events = response['Events']
    for event in events:
        print(event['CloudTrailEvent'])

if __name__ == "__main__":
    process_cloudtrail_logs()
