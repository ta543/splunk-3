from google.cloud import logging

def process_audit_logs():
    client = logging.Client()
    filter_str = 'logName="projects/<your-project-id>/logs/cloudaudit.googleapis.com%2Factivity"'
    for entry in client.list_entries(filter_=filter_str):
        print(entry.payload)

if __name__ == "__main__":
    process_audit_logs()
