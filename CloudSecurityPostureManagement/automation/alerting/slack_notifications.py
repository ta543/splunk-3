import requests
import json

def send_slack_message(webhook_url, message):
    headers = {'Content-Type': 'application/json'}
    data = {"text": message}
    response = requests.post(webhook_url, data=json.dumps(data), headers=headers)
    return response.status_code

if __name__ == "__main__":
    webhook_url = "https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX"
    message = "Alert: Unusual activity detected in the cloud environment."
    send_slack_message(webhook_url, message)
