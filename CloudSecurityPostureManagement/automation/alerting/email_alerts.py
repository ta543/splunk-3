import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(subject, message, to_emails):
    sender_email = "your-email@example.com"
    sender_password = "your-password"
    email = MIMEMultipart()
    email['From'] = sender_email
    email['To'] = ", ".join(to_emails)
    email['Subject'] = subject
    email.attach(MIMEText(message, 'plain'))
    server = smtplib.SMTP('smtp.example.com', 587)
    server.starttls()
    server.login(sender_email, sender_password)
    text = email.as_string()
    server.sendmail(sender_email, to_emails, text)
    server.quit()

if __name__ == "__main__":
    send_email("Security Alert", "A potential security threat has been detected.", ["recipient@example.com"])
