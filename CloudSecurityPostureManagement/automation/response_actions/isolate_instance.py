import boto3

def isolate_instance(instance_id):
    ec2 = boto3.client('ec2')
    ec2.revoke_security_group_ingress(GroupId='sg-12345678', IpPermissions=[{'IpProtocol': 'tcp', 'FromPort': 0, 'ToPort': 65535, 'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}])
    ec2.revoke_security_group_egress(GroupId='sg-12345678', IpPermissions=[{'IpProtocol': 'tcp', 'FromPort': 0, 'ToPort': 65535, 'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}])
    print(f"Instance {instance_id} has been isolated.")

if __name__ == "__main__":
    isolate_instance("i-1234567890abcdef0")
