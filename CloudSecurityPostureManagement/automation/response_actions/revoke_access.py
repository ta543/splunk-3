import boto3

def revoke_user_access(user_name):
    iam = boto3.client('iam')
    iam.delete_access_key(UserName=user_name)
    print(f"Access for user {user_name} has been revoked.")

if __name__ == "__main__":
    revoke_user_access("suspicious_user")
