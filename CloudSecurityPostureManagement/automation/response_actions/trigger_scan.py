import requests

def trigger_scan(target_url):
    scan_api_url = "http://example.com/start_scan"
    response = requests.post(scan_api_url, data={"url": target_url})
    print(f"Scan triggered for {target_url}: {response.text}")

if __name__ == "__main__":
    trigger_scan("http://your-target-url.com")
